# Training with Logistic Regression
- Accuracy Score: 0.5584198192166053
- Classification Report: 
                  precision    recall  f1-score   support
    
              0       0.56      1.00      0.72      1668
              1       0.00      0.00      0.00       723
              2       0.00      0.00      0.00       596
    
    avg / total       0.31      0.56      0.40      2987

- Confusion Metrics: 

                   0    1    2
          0     1668    0    0
          1      723    0    0
          2       596    0    0

- Normalized confusion matrix

                   0            1           2
        0        1.0            0           0
        1        1.0            0           0
        2        1.0            0           0

![Logistic Regression Confussion Matrix](../model/logistic_reg_w2v.png)




# Training Random Forest
- Accuracy Score: 0.7897556076330766
- Classification Report: 
                  precision    recall  f1-score   support
    
              0       0.78      0.98      0.87      1668
              1       0.82      0.50      0.62       723
              2       0.80      0.63      0.70       596
    
    avg / total       0.79      0.79      0.77      2987

- Confusion Metrics: 

                   0    1    2
            0   1627   23   18
            1    286  359   78
            2    165   58  373

- Normalized confusion matrix

                   0            1           2
        0   0.97541966  0.01378897  0.01079137
        1   0.395574    0.49654219  0.10788382
        2   0.27684564  0.09731544  0.62583893

![Random Forest Confussion Matrix](../model/random_forest_w2v.png)
 
# Training Gradient Boosting
- Learning rate:  0.05
Accuracy Score: 0.7977904251757616

- Learning rate:  0.1
Accuracy Score: 0.8188818212253097

- Learning rate:  0.25
Accuracy Score: 0.8269166387679946

- Learning rate:  0.5
Accuracy Score: 0.8228992299966521

- Learning rate:  0.75
Accuracy Score: 0.8316036156678942

- Learning rate:  1
Accuracy Score: 0.8148644124539672

### Training with a learning rate = 0.75
Accuracy Score: 0.8316036156678942

- Classification Report: 
                  precision    recall  f1-score   support
    
              0       0.87      0.93      0.90      1668
              1       0.76      0.67      0.71       723
              2       0.77      0.74      0.76       596
    
    avg / total       0.83      0.83      0.83      2987

- Confusion Metrics: 

                   0    1    2
            0   1558   68   42
            1    151  485   87
            2     74   81  441

- Normalized confusion matrix

                    0           1           2
        0   0.93405276  0.04076739  0.02517986
        1   0.20885201  0.67081604  0.12033195
        2   0.12416107  0.13590604  0.73993289
        
![Gradient Boosting Confussion Matrix](../model/gbt_w2v.png)
