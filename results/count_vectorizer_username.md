# Top 10 relevant users:
 ```
 [('LOCATELVZLA_ATC', 61), ('FUNDATIACRUZ', 60), ('DONAMED_VE', 36), ('BOTMEDX', 23), ('MARACAYACTIVA', 22), ('SERVICIOPUBLIVE', 19), ('TRAFFICVALENCIA', 15), ('TODONOTICIASYA', 11), ('ALEIDABAEZ', 9), ('GABOX_JESUS', 8)] 
 ```


# Training with Logistic Regression
- Accuracy Score: 0.8526950117174422
- Classification Report: 
Classification Report: 
                  precision    recall  f1-score   support
    
              0       0.87      0.96      0.91      1668
              1       0.82      0.68      0.74       723
              2       0.84      0.77      0.80       596
    
    avg / total       0.85      0.85      0.85      2987

- Confusion Metrics: 

                   0    1    2
          0     1597   42   29
          1      171  492   60
          2       73   65  458

- Normalized confusion matrix

                   0            1           2
        0    0.95743405  0.02517986  0.01738609
        1    0.23651452  0.68049793  0.08298755
        2    0.12248322  0.1090604   0.76845638

![Logistic Regression Confussion Matrix](../model/logistic_reg_user.png)




# Training Random Forest
- Accuracy Score: 0.842651489789086
- Classification Report: 
Classification Report: 
                  precision    recall  f1-score   support
    
              0       0.88      0.93      0.91      1668
              1       0.78      0.73      0.75       723
              2       0.79      0.74      0.76       596
    
    avg / total       0.84      0.84      0.84      2987

- Confusion Metrics: 

                   0    1    2
            0   1549   72   47
            1    127  527   69
            2     78   77  441

- Normalized confusion matrix

                   0            1           2
        0   0.92865707  0.04316547  0.02817746
        1   0.17565698  0.72890733  0.09543568
        2   0.13087248  0.12919463  0.73993289

![Random Forest Confussion Matrix](../model/random_forest_user.png)
 
# Training Gradient Boosting
- Learning rate:  0.05
Accuracy Score: 0.8382992969534651

- Learning rate:  0.1
Accuracy Score: 0.855038500167392

- Learning rate:  0.25
Accuracy Score: 0.8600602611315702

- Learning rate:  0.5
Accuracy Score: 0.8627385336457984

- Learning rate:  0.75
Accuracy Score: 0.8526950117174422

- Learning rate:  1
Accuracy Score: 0.8510210913960495

### Training with a learning rate = 0.5
Accuracy Score: 0.8627385336457984

- Classification Report: 
                  precision    recall  f1-score   support
    
              0       0.89      0.95      0.92      1668
              1       0.82      0.73      0.77       723
              2       0.82      0.79      0.80       596
    
    avg / total       0.86      0.86      0.86      2987

- Confusion Metrics: 

                   0    1    2
            0   1578   51   39
            1    127  531   65
            2     62   66  468

- Normalized confusion matrix

                    0           1           2
        0   0.94604317  0.03057554  0.02338129
        1   0.17565698  0.73443983  0.08990318
        2   0.10402685  0.11073826  0.7852349
        
![Gradient Boosting Confussion Matrix](../model/gbt_user.png)
