# Training with Logistic Regression
- Accuracy Score: 0.8778038165383327
- Classification Report: 
                  precision    recall  f1-score   support
    
              precision    recall  f1-score   support

              0       0.87      0.95      0.91      1668
              1       0.80      0.67      0.73       723
              2       0.82      0.76      0.79       596
    
    avg / total       0.84      0.84      0.84      2987

- Confusion Metrics: 

                   0    1    2
          0     1586   52   30
          1      165  486   72
          2       72   72  452

- Normalized confusion matrix

                   0            1           2
        0    0.95083933  0.03117506  0.01798561
        1    0.22821577  0.67219917  0.09958506
        2    0.12080537  0.12080537  0.75838926

![Logistic Regression Confussion Matrix](../model/logistic_reg.png)




# Training Random Forest
- Accuracy Score: 0.8660863742885838
- Classification Report: 
              precision    recall  f1-score   support

          0       0.90      0.92      0.91      1668
          1       0.76      0.74      0.75       723
          2       0.79      0.77      0.78       596

avg / total       0.84      0.84      0.84      2987

- Confusion Metrics: 

                   0    1    2
            0   1530   94   44
            1    114  533   76
            2     62   76  458

- Normalized confusion matrix

                   0            1           2
        0   0.91726619  0.05635492  0.0263789
        1   0.15767635  0.73720609  0.10511757
        2   0.10402685  0.12751678  0.76845638

![Random Forest Confussion Matrix](../model/random_forest.png)
 
# Training Gradient Boosting
- Learning rate:  0.05
Accuracy Score: 0.8272514228322732

- Learning rate:  0.1
Accuracy Score: 0.8403080013391363

- Learning rate:  0.25
Accuracy Score: 0.855038500167392

- Learning rate:  0.5
Accuracy Score: 0.8530297957817208

- Learning rate:  0.75
Accuracy Score: 0.8503515232674924

- Learning rate:  1
Accuracy Score: 0.8456645463675929

### Training with a learning rate = 0.25
Accuracy Score: 0.855038500167392

- Classification Report: 
              precision    recall  f1-score   support

          0       0.89      0.94      0.91      1668
          1       0.79      0.73      0.76       723
          2       0.83      0.77      0.80       596

avg / total       0.85      0.86      0.85      2987

- Confusion Metrics: 

                   0    1    2
            0   1568   63   37
            1    139  527   57
            2     62   75  459

- Normalized confusion matrix

                    0           1           2
        0   0.94004796  0.03776978  0.02218225
        1   0.1922545   0.72890733  0.07883817
        2   0.10402685  0.12583893  0.77013423
        
![Gradient Boosting Confussion Matrix](../model/gbt.png)
