FROM python:3.6-jessie
LABEL python_version=python3.6
LABEL maintainer="albertosadde@gmail.com"

RUN apt-get update

ADD requirements.txt /app/
WORKDIR /app/
RUN pip install -r requirements.txt
RUN python -c "import nltk; nltk.download('stopwords', download_dir='/usr/local/share/nltk_data')"


ADD . /app/
CMD chmod +x ./entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
