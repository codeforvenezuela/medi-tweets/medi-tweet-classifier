from gensim.models.keyedvectors import KeyedVectors
from classifier.data_procesor import only_text
from classifier.trainer import _evaluate_model
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression

from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier

local_path = "../model"


class Sentence2Vec:
    def __init__(self):
        self.model = None
        self.load()

    def load(self):
        wordvectors_file_vec = f"{local_path}/fasttext-sbwc.vec"
        cantidad = 100000
        self.model = KeyedVectors.load_word2vec_format(
            wordvectors_file_vec, limit=cantidad
        )

    #        self.model = Word2Vec.load(model_file)

    def get_vector(self, sentence):
        """ Given a sentence (text) return a vector representation of it using a word2vec model"""

        # convert to lowercase, ignore all special characters - keep only
        # alpha-numericals and spaces
        sentence = only_text(str(sentence))
        vectors = [self.model.wv[w] for w in sentence.split(" ") if w in self.model.wv]

        v = np.zeros(self.model.vector_size)

        if len(vectors) > 0:
            v = (np.array([sum(x) for x in zip(*vectors)])) / v.size

        return v


s2v = Sentence2Vec()


def _read_data(training_file_name):
    df = pd.read_csv(
        training_file_name, header=0, delimiter=";", engine="python", encoding="utf-8"
    )
    clean_train_tweets = []

    for i in range(0, len(df.index)):
        clean_train_tweets.append(s2v.get_vector(df["text"][i]))

    return clean_train_tweets, df["cluster"]


def train_logistic_regression_word2vec_classifier(X_train, y):
    clf_name = "Logistic Regression Word2Vec"
    model_name = "logistic_reg_w2v"
    print(f"=====> Training {clf_name}")

    _evaluate_model(
        clf=LogisticRegression(),
        X=X_train,
        y=y,
        title=f"Count Vectorizer + {clf_name} Classifier",
        img_name=model_name,
    )


#    clf = LogisticRegression().fit(X_train, y)  # Training logistic regression
#    _save_model(clf, vectorizer, model_name)


def train_random_forest_word2vec_classifier(X_train, y):
    clf_name = "Random Forest Word2Vec"
    model_name = "random_forest_w2v"
    print(f"=====> Training {clf_name}")

    _evaluate_model(
        clf=RandomForestClassifier(n_estimators=300),
        X=X_train,
        y=y,
        title=f"Count Vectorizer + {clf_name} Classifier",
        img_name=model_name,
    )


#    clf = LogisticRegression().fit(X_train, y)  # Training logistic regression
#    _save_model(clf, vectorizer, model_name)


def train_gbt_word2vec_classifier(X_train, y):
    clf_name = "Gradient Boosting Word2Vec"
    model_name = "gbt_w2v"

    print(f"=====> Training {clf_name}")

    learning_rates = [0.05, 0.1, 0.25, 0.5, 0.75, 1]
    best_score = 0
    best_learning_rate = 0.05

    for learning_rate in learning_rates:
        gb = GradientBoostingClassifier(
            n_estimators=100, learning_rate=learning_rate, max_depth=3, random_state=0
        ).fit(X_train, y)
        print("Learning rate: ", learning_rate)

        current_accuracy = _evaluate_model(
            clf=gb,
            X=X_train,
            y=y,
            title=f"Count Vectorizer + {clf_name} Classifier",
            img_name=model_name,
            simple_evaluation=True,
        )

        if current_accuracy > best_score:
            best_score = current_accuracy
            best_learning_rate = learning_rate
        print()

    print(f"Training with a learning rate = {best_learning_rate}")
    clf = GradientBoostingClassifier(
        n_estimators=100, learning_rate=best_learning_rate, max_depth=3, random_state=0
    )
    _evaluate_model(
        clf=clf,
        X=X_train,
        y=y,
        title=f"Count Vectorizer + {clf_name} Classifier",
        img_name=model_name,
    )


#    clf = GradientBoostingClassifier(n_estimators=100, learning_rate=best_learning_rate, max_depth=3, random_state=0)
#    _save_model(clf, vectorizer, model_name)


def main():
    tweeter_file_raw = "../data/data_tweets_raw.csv"
    X_train, y = _read_data(tweeter_file_raw)

    # Using model from https://github.com/uchile-nlp/spanish-word-embeddings
    for function_train in [
        train_logistic_regression_word2vec_classifier,
        train_random_forest_word2vec_classifier,
        train_gbt_word2vec_classifier,
    ]:
        function_train(X_train, y)


if __name__ == "__main__":
    main()
