import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_predict
from sklearn.preprocessing import LabelBinarizer

from classifier.data_procesor import pre_process_text

local_path = "../model"

LogisticRegression()


def plot_confusion_matrix(
    y_true, y_pred, classes, normalize=False, title=None, cmap=plt.cm.Blues
):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    Source: https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
    """
    if not title:
        if normalize:
            title = "Normalized confusion matrix"
        else:
            title = "Confusion matrix, without normalization"

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    # classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print("Confusion matrix, without normalization")

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation="nearest", cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(
        xticks=np.arange(cm.shape[1]),
        yticks=np.arange(cm.shape[0]),
        # ... and label them with the respective list entries
        xticklabels=classes,
        yticklabels=classes,
        title=title,
        ylabel="True label",
        xlabel="Predicted label",
    )

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = ".2f" if normalize else "d"
    thresh = cm.max() / 2.0
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(
                j,
                i,
                format(cm[i, j], fmt),
                ha="center",
                va="center",
                color="white" if cm[i, j] > thresh else "black",
            )
    fig.tight_layout()
    return ax


def _evaluate_model(clf, X, y, title, img_name, simple_evaluation=False):
    predicted = cross_val_predict(clf, X, y, cv=3)
    score = metrics.accuracy_score(y, predicted)
    print(f"Accuracy Score: {score}")

    if simple_evaluation:
        return score

    print(f"Classification Report: \n {metrics.classification_report(y, predicted)}")
    print(f"Confusion Metrics: \n {metrics.confusion_matrix(y, predicted)}")
    print()

    plot_confusion_matrix(
        y,
        predicted,
        classes=["UNCLASSIFIED", "SUPPLY", "DEMAND"],
        normalize=True,
        title=f"Confusion matrix: {title}",
    )
    plt.savefig(f"{local_path}/{img_name}.png")


def _read_data(training_file_name):
    df = pd.read_csv(
        training_file_name, header=0, delimiter=";", engine="python", encoding="utf-8"
    )
    clean_train_tweets = []

    for i in range(0, len(df.index)):
        clean_train_tweets.append(pre_process_text(df["text"][i]))

    return clean_train_tweets, df["cluster"]


def _get_training_data(clean_train_tweets):
    vectorizer = CountVectorizer(
        strip_accents="unicode",
        analyzer="word",
        tokenizer=None,
        preprocessor=None,
        stop_words=None,
        max_features=100,
    )

    train_text_features = vectorizer.fit_transform(clean_train_tweets)
    train_text_features = train_text_features.toarray()
    return vectorizer, train_text_features


def _save_model(clf, vectorizer, model_name, label_encoder=None):
    pickle.dump(clf, open(f"{local_path}/{model_name}.model", "wb"))
    feature_list = vectorizer.get_feature_names()
    pickle.dump(feature_list, open(f"{local_path}/{model_name}_vocabulary.pkl", "wb"))

    if label_encoder is not None:
        pickle.dump(
            feature_list, open(f"{local_path}/{model_name}_label_encoder.pkl", "wb")
        )


def train_logistic_regression_classifier(training_file_name):
    clf_name = "Logistic Regression"
    model_name = "logistic_reg"
    print(f"=====> Training {clf_name}")

    clean_train_tweets, y = _read_data(training_file_name)
    vectorizer, X_train = _get_training_data(clean_train_tweets)

    _evaluate_model(
        clf=LogisticRegression(),
        X=X_train,
        y=y,
        title=f"Count Vectorizer + {clf_name} Classifier",
        img_name=model_name,
    )

    clf = LogisticRegression().fit(X_train, y)  # Training logistic regression
    _save_model(clf, vectorizer, model_name)


def train_random_forest_classifier(training_file_name):
    clf_name = "Random Forest"
    model_name = "random_forest"
    print(f"=====> Training {clf_name}")

    clean_train_tweets, y = _read_data(training_file_name)
    vectorizer, X_train = _get_training_data(clean_train_tweets)

    _evaluate_model(
        clf=RandomForestClassifier(n_estimators=300),
        X=X_train,
        y=y,
        title=f"Count Vectorizer + {clf_name} Classifier",
        img_name=model_name,
    )

    clf = RandomForestClassifier(n_estimators=300).fit(
        X_train, y
    )  # Training logistic regression
    _save_model(clf, vectorizer, model_name)


def train_gbt_classifier(training_file_name):
    clf_name = "Gradient Boosting"
    model_name = "gbt"

    print(f"=====> Training {clf_name}")

    clean_train_tweets, y = _read_data(training_file_name)
    vectorizer, X_train = _get_training_data(clean_train_tweets)

    learning_rates = [0.05, 0.1, 0.25, 0.5, 0.75, 1]
    best_score = 0
    best_learning_rate = 0.05

    for learning_rate in learning_rates:
        gb = GradientBoostingClassifier(
            n_estimators=100, learning_rate=learning_rate, max_depth=3, random_state=0
        ).fit(X_train, y)
        print("Learning rate: ", learning_rate)

        current_accuracy = _evaluate_model(
            clf=gb,
            X=X_train,
            y=y,
            title=f"Count Vectorizer + {clf_name} Classifier",
            img_name=model_name,
            simple_evaluation=True,
        )

        if current_accuracy > best_score:
            best_score = current_accuracy
            best_learning_rate = learning_rate
        print()

    print(f"Training with a learning rate = {best_learning_rate}")
    clf = GradientBoostingClassifier(
        n_estimators=100, learning_rate=best_learning_rate, max_depth=3, random_state=0
    )
    _evaluate_model(
        clf=clf,
        X=X_train,
        y=y,
        title=f"Count Vectorizer + {clf_name} Classifier",
        img_name=model_name,
    )

    clf = GradientBoostingClassifier(
        n_estimators=100, learning_rate=best_learning_rate, max_depth=3, random_state=0
    )
    _save_model(clf, vectorizer, model_name)


def _get_username_df(df):
    def _get_top_username(top=10):
        usernames = {}

        for username in df.username:
            if type(username) == str:
                usernames[username.upper()] = usernames.get(username.upper(), 0) + 1

        if len(usernames.items()) < top:
            top = len(usernames.items())

        sorted_usernames = sorted(
            usernames.items(), key=lambda kv: kv[1], reverse=True
        )[:top]

        print(f"Top {top} relevant hashtags {sorted_usernames}")
        return [name for (name, _) in sorted_usernames]

    def _get_username_vector(label_binarizer, username):
        if type(username) == str and len(username.strip()) > 0:
            result = label_binarizer.transform([username.upper()])
            return sum(result)

        return label_binarizer.transform(["test"])[0]

    # Looking the top 10 of hashtags in our dataset and create a LabelBinarizer
    names = _get_top_username(10)
    label_binarizer = LabelBinarizer()
    label_binarizer.fit(names)

    # Create a new dataframe using the new HashTag Encoder
    df["username_tag_vector"] = [
        _get_username_vector(label_binarizer, X) for X in df["username"]
    ]
    username_df = pd.DataFrame(df["username_tag_vector"].values.tolist())

    return username_df, label_binarizer


def _get_training_data_username(df):
    # Procesing the text in our dataset: lower case, remove stop words, stemmer, ...
    df["process_text"] = [pre_process_text(X) for X in df["text"]]

    # Create a new dataframe using CountVectorizer
    vectorizer = CountVectorizer(
        strip_accents="unicode",
        analyzer="word",
        tokenizer=None,
        preprocessor=None,
        stop_words=None,
        max_features=100,
        ngram_range=(1, 2),
    )

    train_text_features = vectorizer.fit_transform(df["process_text"])
    train_text_features = pd.DataFrame(
        train_text_features.toarray(), columns=vectorizer.get_feature_names()
    )
    return vectorizer, train_text_features


def train_logistic_regression_classifier_username(training_file_name):
    clf_name = "Logistic Regression + Username"
    model_name = "logistic_reg_user"
    print(f"=====> Training {clf_name}")

    df = pd.read_csv(
        training_file_name, header=0, delimiter=";", engine="python", encoding="utf-8"
    )

    username_df, label_binarizer = _get_username_df(df)
    vectorizer, train_text_features = _get_training_data_username(df)

    # Create our training dataset
    X_train = pd.concat(
        [train_text_features, username_df], axis=1, join_axes=[username_df.index]
    )
    y = df["cluster"]

    _evaluate_model(
        clf=LogisticRegression(),
        X=X_train,
        y=y,
        title=f"Count Vectorizer + {clf_name} Classifier",
        img_name=model_name,
    )

    clf = LogisticRegression().fit(X_train, y)  # Training logistic regression
    _save_model(clf, vectorizer, model_name, label_binarizer)


def train_random_forest_classifier_username(training_file_name):
    clf_name = "Random Forest + Username"
    model_name = "random_forest_user"
    print(f"=====> Training {clf_name}")

    df = pd.read_csv(
        training_file_name, header=0, delimiter=";", engine="python", encoding="utf-8"
    )

    username_df, label_binarizer = _get_username_df(df)
    vectorizer, train_text_features = _get_training_data_username(df)

    # Create our training dataset
    X_train = pd.concat(
        [train_text_features, username_df], axis=1, join_axes=[username_df.index]
    )
    y = df["cluster"]

    _evaluate_model(
        clf=RandomForestClassifier(n_estimators=300),
        X=X_train,
        y=y,
        title=f"Count Vectorizer + {clf_name} Classifier",
        img_name=model_name,
    )

    clf = RandomForestClassifier(n_estimators=300).fit(X_train, y)  # Training
    _save_model(clf, vectorizer, model_name, label_binarizer)


def train_gbt_classifier_username(training_file_name):
    clf_name = "Gradient Boosting + Username"
    model_name = "gbt_user"
    print(f"=====> Training {clf_name}")

    df = pd.read_csv(
        training_file_name, header=0, delimiter=";", engine="python", encoding="utf-8"
    )

    username_df, label_binarizer = _get_username_df(df)
    vectorizer, train_text_features = _get_training_data_username(df)

    # Create our training dataset
    X_train = pd.concat(
        [train_text_features, username_df], axis=1, join_axes=[username_df.index]
    )
    y = df["cluster"]

    learning_rates = [0.05, 0.1, 0.25, 0.5, 0.75, 1]
    best_score = 0
    best_learning_rate = 0.05

    for learning_rate in learning_rates:
        gb = GradientBoostingClassifier(
            n_estimators=100, learning_rate=learning_rate, max_depth=3, random_state=0
        ).fit(X_train, y)
        print("Learning rate: ", learning_rate)

        current_accuracy = _evaluate_model(
            clf=gb,
            X=X_train,
            y=y,
            title=f"Count Vectorizer + {clf_name} Classifier",
            img_name=model_name,
            simple_evaluation=True,
        )

        if current_accuracy > best_score:
            best_score = current_accuracy
            best_learning_rate = learning_rate
        print()

    print(f"Training with a learning rate = {best_learning_rate}")

    clf = GradientBoostingClassifier(
        n_estimators=100, learning_rate=best_learning_rate, max_depth=3, random_state=0
    )
    _evaluate_model(
        clf=clf,
        X=X_train,
        y=y,
        title=f"Count Vectorizer + {clf_name} Classifier",
        img_name=model_name,
    )

    clf = GradientBoostingClassifier(
        n_estimators=100, learning_rate=best_learning_rate, max_depth=3, random_state=0
    )
    _save_model(clf, vectorizer, model_name, label_binarizer)


def main():
    # tweeter_file_raw = "../data/data_tweets_raw.csv"
    tweeter_file_raw_v2 = "../data/data_tweets_raw_v2.csv"

    for function_train in [  # train_logistic_regression_classifier_username,
        # train_random_forest_classifier_username,
        train_gbt_classifier_username
        # train_logistic_regression_classifier,
        # train_random_forest_classifier,
        # train_gbt_classifier
    ]:
        function_train(tweeter_file_raw_v2)


if __name__ == "__main__":
    main()
