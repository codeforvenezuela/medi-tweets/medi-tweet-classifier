import logging
import os
from datetime import datetime

import flask
from flask import jsonify, request
from flask.logging import default_handler
from pythonjsonlogger import jsonlogger

from services.prediction_service import PredictionService

logger = logging.getLogger()
formatter = jsonlogger.JsonFormatter("%(asctime) %(levelname) %(module) %(funcName) %(lineno) %(message)")
default_handler.setFormatter(formatter)
logger.setLevel(os.environ.get("LOGLEVEL", "DEBUG").upper())
logger.addHandler(default_handler)

app = flask.Flask(__name__)

model_file = os.environ["MEDITWEET_CLASSIFIER_MODEL_FILE"]
vocabulary_file = os.environ["MEDITWEET_CLASSIFIER_VOCABULARY_FILE"]
prediction_service = PredictionService(model_file=model_file,
                                       vocab_file=vocabulary_file,
                                       medicines_csv="./data/medicines.csv")


@app.route("/health", methods=["GET"])
def ping():
    return flask.jsonify({"tms": datetime.now().timestamp()})  # pragma: no cover


@app.route("/api/predictor", methods=["POST"])
def predictor():
    req = request.json["tweets"]
    logger.debug(f"Predict Request: {req}")
    result = prediction_service.predict(req)

    return jsonify(result)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
