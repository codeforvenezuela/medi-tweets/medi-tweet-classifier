import logging
import re

import pandas as pd
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import RegexpTokenizer

from services.base_service import BaseService

logger = logging.getLogger(__name__)


class DataService(BaseService):
    def __init__(self, medicines_csv):
        super().__init__()
        self.medicines = pd.read_csv(medicines_csv, header=0, delimiter=",", engine="python", encoding="utf-8")
        # Obtaining the medicines names from the file
        self.medicines_regex = list(set([w.lower() for w in self.medicines["nombre-marca"]]))
        self.medicines_regex = re.compile(r"\b" + r"\b|\b".join(map(re.escape, self.medicines_regex)) + r"\b")

        self.components = {
            row["nombre-marca"].lower(): row["nombre-activo"].lower().split("-")

            for index, row in self.medicines.iterrows()
        }

        self.ps = SnowballStemmer("spanish")

        self.tokenizer = RegexpTokenizer(r"\w+")
        self.stop_words = ["twitter"]
        self.spanish_stopwords = set(stopwords.words("spanish"))

        self.word2vec_stop_words = set(stopwords.words("spanish"))
        self.word2vec_stop_words.union(set(["http", "twitter"]))

    def only_text(self, text):
        text = text.lower()
        txt = re.sub("[^a-záéíóúüñ,]", " ", text)
        txt = re.sub(self.medicines_regex, "medicina", txt)
        words = self.tokenizer.tokenize(txt)
        words = [w for w in words if w not in self.word2vec_stop_words and len(w) > 2]

        return " ".join(words)

    def pre_process_text(self, text):
        text = text.lower()
        txt = re.sub("[^a-záéíóúüñ,]", " ", text)
        txt = re.sub(self.medicines_regex, "medicina", txt)
        words = self.tokenizer.tokenize(txt)
        words = [w for w in words if w not in self.spanish_stopwords]
        words = [self.ps.stem(w) for w in words]
        logger.debug(words)

        return " ".join(words)

    def get_medicines(self, text):
        return re.findall(self.medicines_regex, text)


def pre_process_df(local_path):
    data_service = DataService("data/medicines.csv")
    tweeter_file_name = f"{local_path}/data_tweets.csv"
    tweeter_file_raw = f"{local_path}/data_tweets_raw_v2.csv"

    twitter_data_raw = pd.read_csv(tweeter_file_name, header=0, delimiter=";", engine="python", encoding="utf-8")
    twitter_data_raw["text"] = [BeautifulSoup(X).getText().lower() for X in twitter_data_raw["text"]]
    twitter_data_raw["medicines"] = [data_service.get_medicines(X) for X in twitter_data_raw["text"]]

    # ['username', 'date', 'retweets', 'favorites', 'text', 'geo', 'mentions', 'hashtags', 'id', 'permalink',
    # 'cluster', 'text_process', 'medicines']
    print(f"Columns in the dataframe {list(twitter_data_raw.columns.values)}")

    twitter_data_raw.to_csv(
        tweeter_file_raw,
        mode="w",
        columns=["username", "text", "hashtags", "medicines", "cluster"],
        index=False,
        sep=";",
    )


def main():
    pre_process_df("data")


if __name__ == "__main__":
    main()
