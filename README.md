# medi-tweet-classifier

To run this project you need:

1.- Start the container:
```
docker-compose up

```

You can make request to your local service:
```
curl -d '{ "tweets": [{ "id": "ABC", "username": "name", "raw_tweet": "#ServicioPúblico Para paciente psiquiátrico se requiere de los siguientes medicamentos: CELEPID de 25 mgs,  Risperidona de 3 mgs y Alprazolam de 2 mgs. Cualquier información favor comunicarse al 0414-2536502 #12Abr", "hash_tags": ["#ServicioPúblico"]}]}' -X POST http://localhost:8080/api/predictor -H "Content-Type: application/json"

```
